#include "data_reader.hh"

#include <fstream>
#include <iostream>

void reduce() {
  std::ifstream ifs("data_one_column.dat");
  data_reader<int> reader(ifs);

  auto res = reader.reduce(std::plus<>{});

  std::cout << res << '\n';
}

int main() {
  reduce();
}
