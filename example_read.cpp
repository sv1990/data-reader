#include "data_reader.hh"

#include <fstream>
#include <iostream>

void usage1() {
  std::cout << "Usage 1\n";
  std::ifstream ifs("data.dat");
  data_reader<int, double, std::string> reader(ifs);

  for (auto& [i, d, s] : reader) {
    std::cout << i << ' ' << d << ' ' << s << '\n';
  }
  std::cout << '\n';
}

void usage2() {
  std::cout << "Usage 2\n";
  std::ifstream ifs("data.dat");
  data_reader<int, double, std::string> reader(ifs);

  auto data = reader.to_vector();
  for (const auto& [i, d, s] : data) {
    std::cout << i << ' ' << d << ' ' << s << '\n';
  }
  std::cout << '\n';
}

struct A {
  int i;
  double d;
  std::string s;
};

void usage3() {
  std::cout << "Usage 3\n";
  std::ifstream ifs("data.dat");
  data_reader<int, double, std::string> reader(ifs);

  auto data = reader.to_vector<A>();
  for (const auto& [i, d, s] : data) {
    std::cout << i << ' ' << d << ' ' << s << '\n';
  }
  std::cout << '\n';
}

int main() {
  usage1();
  usage2();
  usage3();

  std::ifstream ifs;
  data_reader<int> reader{ifs};
  static_assert(std::is_same_v<decltype(reader.to_vector()), std::vector<int>>);
}
