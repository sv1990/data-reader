#include "data_reader.hh"

#include <fstream>
#include <iostream>

void map() {
  std::ifstream ifs("data.dat");
  data_reader<int, double, std::string> reader(ifs);

  auto vec = reader.map_to_vector(
      [](int i, double d, std::string s) { return std::make_tuple(i + d, s); });

  for (auto& [n, s] : vec) {
    std::cout << n << ' ' << s << '\n';
  }
  std::cout << '\n';
}

void map_reduce() {
  std::ifstream ifs("data.dat");
  data_reader<int, double, std::string> reader(ifs);

  auto res = reader.map_reduce([](int i, double d, std::string){return i + d;}, std::plus<>{});

  std::cout << res << '\n';
}

int main() {
  map();
  map_reduce();
}
