#ifndef DATAREADER_DATA_READER_HH_1531473448755278619_
#define DATAREADER_DATA_READER_HH_1531473448755278619_

#include <algorithm>
#include <functional>
#include <iosfwd>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <vector>

#include <cctype>

template <typename... Ts>
class data_reader {
private:
  template <typename T, typename...>
  struct head {
    using type = T;
  };
  template <typename... Us>
  using head_t = typename head<Us...>::type;

public:
  using value_type = std::tuple<Ts...>;
  using return_value_type =
      std::conditional_t<(sizeof...(Ts) > 1), value_type, head_t<Ts...>>;
  using iterator_category = std::input_iterator_tag;

private:
  value_type _curr_row;
  std::string_view _comment;
  std::istream* _is;

  void trim_comments(std::string& line) noexcept {
    const auto pos = line.find(_comment);
    if (pos == std::string::npos) {
      return;
    }
    line.erase(next(std::begin(line), pos), std::end(line));
  }

  void read_next_line_into_tuple() noexcept {
    for (std::string line; std::getline(*_is, line);) {
      if (!empty(_comment)) {
        trim_comments(line);
      }
      if (std::all_of(std::begin(line), std::end(line), [](auto c) noexcept {
            return std::isspace(c);
          })) {
        continue;
      }
      std::istringstream iss{line};
      std::apply([&iss](auto&... xs) { (iss >> ... >> xs); }, _curr_row);
      if (iss.fail()) {
        _is->setstate(std::ios::failbit);
      }
      break;
    }
  }

public:
  class sentinel {};

  data_reader(std::istream& is, const char* comment = "#") noexcept
      : _comment(comment), _is(std::addressof(is)) {
    read_next_line_into_tuple();
  }

  void set_comment(const char* comment) noexcept { _comment = comment; }

  value_type& operator*() noexcept { return _curr_row; }

  data_reader& operator++() noexcept {
    read_next_line_into_tuple();
    return *this;
  }

  bool operator!=(sentinel) const noexcept { return static_cast<bool>(*_is); }
  bool operator==(sentinel) const noexcept { return !(*this == sentinel{}); }

  data_reader begin() const noexcept { return *this; }
  sentinel end() const noexcept { return {}; }

  template <typename S = return_value_type>
  std::vector<S> to_vector(std::size_t reserve_hint = 256) noexcept {
    return map_to_vector([](auto&&... xs) { return S{std::move(xs)...}; },
                         reserve_hint);
  }

  template <typename Func, typename S = std::invoke_result_t<Func, Ts...>>
  std::vector<S> map_to_vector(Func&& f, std::size_t reserve_hint = 256) {
    std::vector<S> _data;
    _data.reserve(reserve_hint);
    for (auto& row : *this) {
      std::apply(
          [&f, &_data](auto&... xs) {
            _data.push_back(std::invoke(f, std::move(xs)...));
          },
          row);
    }
    return _data;
  }

  template <typename Mapper, typename Reducer>
  std::invoke_result_t<Mapper, Ts...> map_reduce(Mapper&& mapper,
                                                 Reducer&& reducer) {
    auto beg = std::begin(*this);
    auto acc = std::apply(mapper, std::move(*beg));
    while (++beg != std::end(*this)) {
      acc = std::invoke(reducer, std::move(acc),
                        std::apply(mapper, std::move(*beg)));
    }
    return acc;
  }

  template <typename Reducer>
  return_value_type reduce(Reducer&& reducer) {
    auto id = [](auto&&... xs) -> return_value_type {
      return {std::forward<Ts>(xs)...};
    };
    return map_reduce(id, std::forward<Reducer>(reducer));
  }
};

#endif // DATAREADER_DATA_READER_HH_1531473448755278619_
